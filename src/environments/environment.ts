// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyA-sYnXi3IvyXDgl4MHXwUVcPCOtRYtyNU",
        authDomain: "covid-51a75.firebaseapp.com",
        databaseURL: "https://covid-51a75.firebaseio.com",
        projectId: "covid-51a75",
        storageBucket: "covid-51a75.appspot.com",
        messagingSenderId: "87517649811",
        appId: "1:87517649811:web:f5727ff0938ba36994e666",
        measurementId: "G-VMYMPD6LNS"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
