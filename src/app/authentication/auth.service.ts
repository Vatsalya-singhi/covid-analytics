import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';

import * as firebase from 'firebase';

import { Observable, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

import { FirestoreService } from '../common/firestore.service';
import { User } from '../common/model/user.model';

import { auth } from 'firebase/app';


@Injectable({
    providedIn: 'root'
})


export class AuthService {

    public user$: Observable<User>;
    public user: User = null;

    constructor(
        public afAuth: AngularFireAuth,
        private db: AngularFirestore,
        public router: Router,
        public route: ActivatedRoute,
        public storage: AngularFireStorage,
        public firestore: FirestoreService,
    ) {
        this.user$ = this.afAuth.authState.pipe(
            switchMap(user => {
                if (user) {
                    console.log('user=>', user);
                    return this.db.doc<User>(`User/${user.uid}`).valueChanges()
                } else {
                    return of(null);
                }
            }),
            tap(async (userObject: User) => {
                console.log('userObject=>', userObject);
                this.user = userObject;
            })
        )
    }


    /**************************************************************
     ****************Public Functions*****************************
    *************************************************************/

    public async googleSignin(): Promise<any> {
        const provider = new firebase.auth.GoogleAuthProvider();
        provider.setCustomParameters({
            prompt: 'select_account'
        });

        return this.afAuth.signInWithPopup(provider).then((signInRes: any) => {
            const credentials = signInRes;
            console.log('credentials=>', credentials.user);
            this.updateUserData(credentials.user);
            return credentials.user;
        });

    }

    public signInWithEmail(credentials: any): Promise<firebase.User> {
        console.log('Sign in with email');
        return this.afAuth.signInWithEmailAndPassword(credentials.email,
            credentials.password).then((credentials: auth.UserCredential) => {
                console.log('credentials=>', credentials.user);
                // this.updateUserData(credentials.user);
                return credentials.user;
            });
    }

    public registerWithEmail(credentials: any): Promise<void> {
        console.log('Register with email');

        return this.afAuth.createUserWithEmailAndPassword(credentials.email, credentials.password).then((credentials: auth.UserCredential) => {
            this.updateUserData(credentials.user);
        });

    }

    public updatePassword(credentials: any): Promise<void> {
        console.log('Register with email');
        console.log('curr user =>', firebase.auth().currentUser);
        return firebase.auth()
            .currentUser.updatePassword(credentials.password);
    }

    public async signOut(): Promise<any> {
        localStorage.clear();
        await this.afAuth.signOut();

        this.router.navigate(['/login']);

        // setTimeout(() => {
        //     window.location.href = "#";
        // }, 1000);

    }

    public checkAuthorisation(collectionName: string, email: string): Observable<any> {
        return this.firestore.queryDocWithMatchingField('User', `${collectionName}`, 'email', '==', email);
    }

    private updateUserData(credentials: any): void {
        const userRef: AngularFirestoreDocument<any> = this.db.doc(`User/${credentials.uid}`);
        const data = new User({
            id: credentials.uid,
            email: credentials.email,
            name: credentials.displayName,
            createdBy: credentials.uid,
            updatedBy: credentials.uid,
            photoURL: credentials.photoURL,
        });

        console.log('data=>', data);
        this.user = new User(data);
        userRef.set({
            ...data
        }, { merge: true });
    }
}