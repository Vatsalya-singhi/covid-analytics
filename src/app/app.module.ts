import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';

import { LoginModule } from './authentication/login/login.module';
import { DashboardModule } from './dashboard/dashboard.module';

import { FirestoreService } from './common/firestore.service';
import { AuthService } from './authentication/auth.service';
import { environment } from 'src/environments/environment';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,


        LoginModule,
        DashboardModule,

        AppRoutingModule,

        AngularFireModule.initializeApp(environment.firebaseConfig),

        AngularFireAuthModule,
        AngularFirestoreModule,
        AngularFireStorageModule,
    ],
    providers: [
        FirestoreService,
        AuthService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
